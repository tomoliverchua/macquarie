package com.tomchua.filipinoai.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tomchua.filipinoai.Models.CartItemObject;
import com.tomchua.filipinoai.Models.ItemObject;
import com.tomchua.filipinoai.Object.ProductObject;
import com.tomchua.filipinoai.R;
import com.tomchua.filipinoai.adapter.ProductListAdapter;
import com.tomchua.filipinoai.adapter.RouteListAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Routes extends AppCompatActivity implements RecognitionListener {

    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";
    private ToggleButton BtnVoiceRecognition;

    TextToSpeech t1;
    private ProgressBar progressBar;

    ListView ListRoutes;
    private List<Data> mCartItemObjects = new ArrayList<>();
    Context context = this;
    private RouteListAdapter mAdapter;
    String voicetText = "";
    Boolean isChecked = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_routes);
        InitVariables();
        SpeechTrigger();
//        String[] productName = {"Station 1", "Station 2", "Station 3", "Station 4", "Station 5", "Station 6", "Station 7", "Station 8"};
//        for (int x = 0; x < productName.length; x++) {
//            ProductObject obj = new ProductObject();
//            obj.setProduct_name(productName[x]);
//
//            mCartItemObjects.add(obj);
//
//            Log.d("test", "" + mCartItemObjects.get(x).getProduct_name().toString());
//        }


//
        BtnVoiceRecognition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setIndeterminate(true);
                    speech.startListening(recognizerIntent);
                } else {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    speech.stopListening();
                }
            }
        });
        GetDataFirebase();

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    Locale locale = new Locale("en","US");
                    Locale.setDefault(locale);
                    t1.setLanguage(locale);
                }
            }
        });
    }

    private void GetDataFirebase(){
        String getProductname = getIntent().getStringExtra("example");
        DatabaseReference db = FirebaseDatabase.getInstance().getReference(getProductname);
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCartItemObjects.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    Data data = ds.getValue(Data.class);
                    Log.d("Data",data.Address);
                    mCartItemObjects.add(data);
                }
                mAdapter = new RouteListAdapter(context, mCartItemObjects);
                ListRoutes.setAdapter(mAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("DataError",databaseError.getMessage());
            }
        });
    }

    private void InitVariables(){
        ListRoutes = (ListView) findViewById(R.id.list_routes);
        BtnVoiceRecognition = (ToggleButton)findViewById(R.id.toggleButton1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.destroy();
            Log.i(LOG_TAG, "destroy");
        }

        if(t1 != null){

            t1.stop();
            t1.shutdown();
        }
        super.onPause();

    }



    @Override
    public void onBeginningOfSpeech() {
        Log.i(LOG_TAG, "onBeginningOfSpeech");
        progressBar.setIndeterminate(false);
        progressBar.setMax(3);
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.i(LOG_TAG, "onBufferReceived: " + buffer);
    }

    @Override
    public void onEndOfSpeech() {
        Log.i(LOG_TAG, "onEndOfSpeech");
        progressBar.setIndeterminate(true);
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(LOG_TAG, "FAILED " + errorMessage);
        Toast.makeText(context, "" + errorMessage.toString(), Toast.LENGTH_SHORT).show();
        BtnVoiceRecognition.setChecked(false);
    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {
        Log.i(LOG_TAG, "onEvent");
    }

    @Override
    public void onPartialResults(Bundle arg0) {
        Log.i(LOG_TAG, "onPartialResults");
    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {
        Log.i(LOG_TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.i(LOG_TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        for (String result : matches)
            text += result + "\n";


        voicetText = matches.get(0).toString();
        if(voicetText.contains("macquarie")){
            String response = "Hi what can do for you";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);

        }
        if(voicetText.contains("san francisco to maharlika")){
            String response = "You choose San Francisco to E Pantaleon route";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
            showEnterAmountDialog();

        }
        if(voicetText.contains("san Francisco to pantaleon")){
            String response = "You choose San Francisco to E Pantaleon route";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        }
        if(voicetText.contains("san francisco to busilak")){
            String response = "You choose San Francisco to  Busilak route";
            t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        }
//        Toast.makeText(this, "" + matches.get(0).toString(), Toast.LENGTH_SHORT).show();

    }


    // Method to show transaction dialog
    private void showEnterAmountDialog() {
        String response = "Please enter your amount";
        t1.speak(response, TextToSpeech.QUEUE_FLUSH, null);
        final Dialog dialog = new Dialog(this, android.R.style.Theme_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_enteramount);

        TextView textTotalAmount = (TextView) dialog.findViewById(R.id.text_totalamount);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
        TextView btnProceed = (TextView) dialog.findViewById(R.id.btn_proceed);
        final EditText inputAmount = (EditText) dialog.findViewById(R.id.text_cashamount);
        final TextView textChange = (TextView) dialog.findViewById(R.id.text_change);


        textChange.setText("₱0.00");
        textTotalAmount.setText(getString(R.string.peso) + "100.00");

        inputAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Double change = Double.parseDouble(charSequence.toString().isEmpty() ? "0" : charSequence.toString()) - 100;
                textChange.setText(getString(R.string.peso) + new DecimalFormat("###,##0.00").format(change < 0.0 ? 0.0 : change));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Double change = Double.parseDouble(textChange.getText().toString().substring(1, textChange.getText().toString().length()));
//                Double cashAmount = Double.parseDouble(inputAmount.getText().toString().isEmpty() ? "0" : inputAmount.getText().toString());
//                if (cashAmount >= totalCashPayment) {
                startActivity(new Intent(context,FingerPrintActivity.class));
//                }
//                else Toast.makeText(ShoppingActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        Log.i(LOG_TAG, "onRmsChanged: " + rmsdB);
        progressBar.setProgress((int) rmsdB);
    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    //
    private void SpeechTrigger(){
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,"fil_PH");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
    }
}
