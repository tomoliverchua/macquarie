package com.tomchua.filipinoai.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tomchua.filipinoai.Activity.Data;
import com.tomchua.filipinoai.Models.CartItemObject;
import com.tomchua.filipinoai.Object.ProductObject;
import com.tomchua.filipinoai.R;

import java.util.List;

public class RouteListAdapter extends BaseAdapter {
    private List<Data> mCartItemObjects;

    private LayoutInflater mInflater;
    private Context mContext;

    public RouteListAdapter(Context context, List<Data> objects) {
        this.mContext = context;
        this.mCartItemObjects = objects;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mCartItemObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return mCartItemObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.route_items, null);

        TextView textName = (TextView) view.findViewById(R.id.station_name);
        TextView textPrice = (TextView) view.findViewById(R.id.text_itemprice);
        TextView textQty = (TextView) view.findViewById(R.id.text_qty);
        TextView textTotalAmount = (TextView) view.findViewById(R.id.text_totalamount);

        textName.setText(mCartItemObjects.get(i).Address);


        return view;
    }
}
